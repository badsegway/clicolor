# clicolor

Stupid-simple single go file library to add color to your CLI prints via color commands: Info, Warning, Success, Error, ...

## example

```go
package main

import "gitlab.com/badsegway/clicolor"

func main() {
    clicolor.SetBackground("Green")
    clicolor.LogColor("Black", "Golang!\n")
    clicolor.SetEnd()
}
```