package clicolor

import (
	"fmt"
	golog "log"
)

var Codes = map[string]uint8{
	// Standard Colors
	"Black":  0,
	"Red":    1,
	"Green":  2,
	"Yellow": 3,
	"Blue":   4,
	"Purple": 5,
	"Cyan":   6,
	"White":  7,
	"Grey":   8,
	// High-Intensity colors
	"LRed":    9,
	"LGreen":  10,
	"LYellow": 11,
	"LBlue":   12,
	"LPurple": 13,
	"LCyan":   14,
	"LWhite":  15,
}

func set(color interface{}, preCmd string) string {
	var cmdStr string
	argType := fmt.Sprintf("%T", color)
	if argType == "string" {
		cmdStr = fmt.Sprintf("%s%dm", preCmd, Codes[color.(string)])
	} else if argType == "int" {
		cmdStr = fmt.Sprintf("%s%dm", preCmd, color.(int))
	}
	return cmdStr
}

func log(color interface{}, format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s",
		SetForeground(color),
		fmt.Sprintf(format, a...),
		End())
}
func SetForeground(color interface{}) string {
	return set(color, "\033[1;38;05;")
}
func SetBackground(color interface{}) string {
	return set(color, "\033[1;48;05;")
}
func End() string {
	return "\033[0m"
}
func LogColor(color interface{}, format string, a ...interface{}) {
	golog.Printf(log("Green", format, a...))
}
func LogSuccess(format string, a ...interface{}) {
	golog.Printf(log("Green", format, a...))
}
func LogError(format string, a ...interface{}) {
	golog.Printf(log("Red", format, a...))
}
func LogWarning(format string, a ...interface{}) {
	golog.Printf(log("Yellow", format, a...))
}
func LogInfo(format string, a ...interface{}) {
	golog.Printf(log("Cyan", format, a...))
}
func LogComment(format string, a ...interface{}) {
	golog.Printf(log("Blue", format, a...))
}
func Println(color interface{}, msg string) {
	fmt.Println(log(color, msg))
}
func Print(color interface{}, format string, a ...interface{}) {
	fmt.Printf(log(color, format, a...))
}
